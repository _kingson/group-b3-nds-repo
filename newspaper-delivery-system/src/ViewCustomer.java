import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Button;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;

public class ViewCustomer {

	private JFrame frame;
	private JTextField textField;
	private Customer customer;
	private JLabel lblNewLabel;
	private JTextArea txtrSearchForCustomer;
	private JLabel lblSearchForCustomer;
	private JTextArea textArea;
	private JLabel lblDob;
	private JLabel lblSearchForCustomer_2;
	private JLabel lblSearchForCustomer_1;
	private JLabel lblSearchForCustomer_3;
	private Customer[] cust;
	private Button button_1;
	private Button button_2;
	private Button button_3;

	/**
	 * Launch the application.
	 */
	public static void ViewCustomersScreen(DatabaseConnector dbobj) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewCustomer window = new ViewCustomer(dbobj);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @param dbobj
	 */
	public ViewCustomer(DatabaseConnector dbobj) {
		initialize(dbobj);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(DatabaseConnector dbobj) {
		frame = new JFrame("View Customer");
		frame.setBounds(100, 100, 508, 558);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		customer = new Customer(dbobj);

		JLabel lblViewCustomers = new JLabel("View Customers");
		lblViewCustomers.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblViewCustomers.setBounds(164, 11, 165, 31);
		frame.getContentPane().add(lblViewCustomers);

		textField = new JTextField();
		textField.setBounds(105, 68, 224, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		JLabel lblName = new JLabel("Customer:");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblName.setBounds(10, 68, 78, 17);
		frame.getContentPane().add(lblName);

		Button button = new Button("Search");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (!textField.getText().equals("")) {
						ResultSet rs = customer.viewCustomerDetails(textField.getText());

						while (rs.next()) {
							lblNewLabel.setText(rs.getString("last_name") + " " + rs.getString("other_names"));
							lblSearchForCustomer_2.setText(rs.getString("date_of_birth"));
							txtrSearchForCustomer.setText(rs.getString("address"));
							lblSearchForCustomer_1.setText(rs.getString("region"));
							lblSearchForCustomer.setText(rs.getString("phone_number"));
							lblSearchForCustomer_3.setText(rs.getString("delivery_frequency"));
							textArea.setText(dbobj.retrieveCustomerSubscriptions(Integer.parseInt(rs.getString("id"))));

							if (rs.isClosed()) {
								break;
							}
						}
						rs = customer.viewCustomerDetails(textField.getText());

						// lblNewLabel.setText(cust[0].lastName + " " + cust[0].otherNames);
						// lblDob.setText(cust[0].dateOfBirth);
						// txtrSearchForCustomer.setText(cust[0].address);
						// label_2.setText(cust[0].region);
						// label.setText(cust[0].phoneNumber);
						// textArea.setText(cust[0].customerPublications);
						// label_3.setText(cust[0].deliveryFrequency);
						// if (cust.length > 1) {
						// button_2.setEnabled(true);
						// } else {
						// button_2.setEnabled(false);
						// }

					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		button.setBounds(351, 68, 70, 22);
		frame.getContentPane().add(button);

		JLabel lblName_1 = new JLabel("Name:");
		lblName_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblName_1.setBounds(10, 145, 78, 17);
		frame.getContentPane().add(lblName_1);

		JLabel lblDetails = new JLabel("Details");
		lblDetails.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblDetails.setBounds(202, 99, 78, 31);
		frame.getContentPane().add(lblDetails);

		lblNewLabel = new JLabel("Search for customer...");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel.setBounds(175, 147, 224, 14);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblAddress = new JLabel("Address:");
		lblAddress.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblAddress.setBounds(10, 201, 78, 17);
		frame.getContentPane().add(lblAddress);

		txtrSearchForCustomer = new JTextArea();
		txtrSearchForCustomer.setEnabled(false);
		txtrSearchForCustomer.setLineWrap(true);
		txtrSearchForCustomer.setText("Search for customer...");
		txtrSearchForCustomer.setBounds(175, 199, 189, 60);
		frame.getContentPane().add(txtrSearchForCustomer);

		JLabel lblPhone = new JLabel("Phone:");
		lblPhone.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPhone.setBounds(10, 299, 78, 17);
		frame.getContentPane().add(lblPhone);

		lblSearchForCustomer = new JLabel("Search for customer...");
		lblSearchForCustomer.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblSearchForCustomer.setBounds(175, 301, 224, 14);
		frame.getContentPane().add(lblSearchForCustomer);

		textArea = new JTextArea();
		textArea.setText("Search for customer...");
		textArea.setLineWrap(true);
		textArea.setEnabled(false);
		textArea.setBounds(175, 326, 189, 81);
		frame.getContentPane().add(textArea);

		JLabel lblPublications = new JLabel("Publications:");
		lblPublications.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPublications.setBounds(10, 327, 96, 17);
		frame.getContentPane().add(lblPublications);

		button_3 = new Button("<<Return");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					CustChoose nw = new CustChoose(dbobj);
					nw.NewScreen4(dbobj);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				try {
					frame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		button_3.setBounds(60, 477, 86, 22);
		frame.getContentPane().add(button_3);

		lblDob = new JLabel("DOB:");
		lblDob.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDob.setBounds(10, 173, 78, 17);
		frame.getContentPane().add(lblDob);

		lblSearchForCustomer_2 = new JLabel("Search for customer...");
		lblSearchForCustomer_2.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblSearchForCustomer_2.setBounds(175, 174, 224, 14);
		frame.getContentPane().add(lblSearchForCustomer_2);

		JLabel lblRegion = new JLabel("Region:");
		lblRegion.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblRegion.setBounds(10, 271, 78, 17);
		frame.getContentPane().add(lblRegion);

		lblSearchForCustomer_1 = new JLabel("Search for customer...");
		lblSearchForCustomer_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblSearchForCustomer_1.setBounds(175, 273, 224, 14);
		frame.getContentPane().add(lblSearchForCustomer_1);

		JLabel lblDeliveryFrequency = new JLabel("Delivery Frequency:");
		lblDeliveryFrequency.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDeliveryFrequency.setBounds(10, 426, 136, 17);
		frame.getContentPane().add(lblDeliveryFrequency);

		lblSearchForCustomer_3 = new JLabel("Search for customer...");
		lblSearchForCustomer_3.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblSearchForCustomer_3.setBounds(175, 428, 224, 14);
		frame.getContentPane().add(lblSearchForCustomer_3);
	}
}
