import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Button;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;

public class EditDeliveryPerson {

	private JFrame frame;
	private JTextField textField;
	private DeliveryPerson deliveryperson;
	ResultSet rs = null;

	/**
	 * Launch the application.
	 */
	public static void EditDeliveryPersonScreen(DatabaseConnector dbobj) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EditDeliveryPerson window = new EditDeliveryPerson(dbobj);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @param dbobj
	 */
	public EditDeliveryPerson(DatabaseConnector dbobj) {
		initialize(dbobj);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(DatabaseConnector dbobj) {
		frame = new JFrame("Edit Delivery Person");
		frame.setBounds(10, 10, 508, 589);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		deliveryperson = new DeliveryPerson(dbobj);

		JLabel lblViewCustomers = new JLabel("Edit/Remove Delivery Person");
		lblViewCustomers.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblViewCustomers.setBounds(72, 11, 292, 31);
		frame.getContentPane().add(lblViewCustomers);

		textField = new JTextField();
		textField.setBounds(140, 68, 189, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		JLabel lblName = new JLabel("Delivery Person:");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblName.setBounds(10, 68, 118, 17);
		frame.getContentPane().add(lblName);

		JLabel lblName_1 = new JLabel("Name:");
		lblName_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblName_1.setBounds(10, 172, 78, 17);
		frame.getContentPane().add(lblName_1);

		JLabel lblDetails = new JLabel("Details");
		lblDetails.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblDetails.setBounds(202, 111, 78, 31);
		frame.getContentPane().add(lblDetails);

		JTextArea name_box = new JTextArea("Search for delivery person...");
		name_box.setFont(new Font("Tahoma", Font.PLAIN, 13));
		name_box.setBounds(140, 175, 224, 20);
		frame.getContentPane().add(name_box);

		JLabel lblAddress = new JLabel("Address:");
		lblAddress.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblAddress.setBounds(10, 270, 78, 17);
		frame.getContentPane().add(lblAddress);

		JTextArea address_box = new JTextArea();
		address_box.setEnabled(true);
		address_box.setLineWrap(true);
		address_box.setText("Search for delivery person...");
		address_box.setBounds(140, 268, 189, 60);
		frame.getContentPane().add(address_box);

		JLabel lblPhone = new JLabel("Phone:");
		lblPhone.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPhone.setBounds(10, 349, 78, 17);
		frame.getContentPane().add(lblPhone);

		JTextArea phoneNumber_box = new JTextArea("Search for delivery person...");
		phoneNumber_box.setFont(new Font("Tahoma", Font.PLAIN, 13));
		phoneNumber_box.setBounds(140, 351, 224, 20);
		frame.getContentPane().add(phoneNumber_box);

		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblUsername.setBounds(10, 218, 96, 17);
		frame.getContentPane().add(lblUsername);

		JTextArea username_box = new JTextArea("Search for delivery person...");
		username_box.setFont(new Font("Tahoma", Font.PLAIN, 13));
		username_box.setBounds(140, 221, 224, 20);
		frame.getContentPane().add(username_box);

		Button button_3 = new Button("Return");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					DeliveryChoose nw = new DeliveryChoose(dbobj);
					nw.NewScreen5(dbobj);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				try {
					frame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}

			}
		});
		button_3.setBounds(34, 461, 86, 22);
		frame.getContentPane().add(button_3);

		JLabel lblAssignedRegion = new JLabel("Assigned Region:");
		lblAssignedRegion.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblAssignedRegion.setBounds(10, 402, 118, 17);
		frame.getContentPane().add(lblAssignedRegion);

		JTextArea assignedRegions_box = new JTextArea("Search for delivery person...");
		assignedRegions_box.setFont(new Font("Tahoma", Font.PLAIN, 13));
		assignedRegions_box.setBounds(140, 405, 224, 20);
		frame.getContentPane().add(assignedRegions_box);

		Button button = new Button("Search");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					if (!textField.getText().equals("")) {
						rs = deliveryperson.retrieveDeliveryPerson(textField.getText());

						while (rs.next()) {
							name_box.setText(rs.getString("name"));
							username_box.setText(rs.getString("username"));
							phoneNumber_box.setText(rs.getString("phone_number"));
							address_box.setText(rs.getString("address"));
							assignedRegions_box.setText(rs.getString("assigned_regions"));

							if (rs.isLast()) {
								rs.beforeFirst();
								break;
							}

						}
					}
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		button.setBounds(351, 68, 70, 22);
		frame.getContentPane().add(button);

		Button button_2 = new Button("Edit");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String r = "";
					// Custom button text
					Object[] options = { "Yes, Edit", "No, Cancel", };
					int n = JOptionPane.showOptionDialog(frame,
							"Are you sure that you want to Edit the Delivery Person ", "Warning",
							JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);

					if (n == 0) {

						rs.beforeFirst();
						while (rs.next()) {
							r = deliveryperson.updateDeliveryPerson(rs.getInt("id"), name_box.getText(),
									username_box.getText(), rs.getString("password"), phoneNumber_box.getText(),
									address_box.getText(), assignedRegions_box.getText());
							JOptionPane.showMessageDialog(null, r);
							if (r == "Delivery Person details successfully updated.") {
								textField.setText("");
								name_box.setText("");
								username_box.setText("");
								phoneNumber_box.setText("");
								address_box.setText("");
								assignedRegions_box.setText("");
								break;
							} else if (!r.equals("")) {
								break;
							}

						}
					} else if (n == 1) {

					}
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		button_2.setBounds(335, 461, 86, 22);
		frame.getContentPane().add(button_2);

		Button button_1 = new Button("Delete");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (textField.getText() == "") {
					JOptionPane.showMessageDialog(null, "No Delivery Person Selected");
				} else {
					try {
						String r = "";

						// Custom button text
						Object[] options = { "Yes, Delete", "No, Cancel", };
						int n = JOptionPane.showOptionDialog(frame,
								"Are you sure that you want to Delete the Delivery Person ", "Warning",
								JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);

						if (n == 0) {
							rs.beforeFirst();
							while (rs.next()) {
								r = deliveryperson.deleteDeliveryPerson(rs.getInt("id"));
								JOptionPane.showMessageDialog(null, r);
								if (r == "Delivery Person has been removed.") {
									textField.setText("");
									name_box.setText("");
									username_box.setText("");
									phoneNumber_box.setText("");
									address_box.setText("");
									assignedRegions_box.setText("");
									break;
								} else if (!r.equals("")) {
									break;
								}

							}
						} else if (n == 1) {

						}
					}

					catch (Exception ex1) {
						ex1.printStackTrace();
					}
				}

			}
		});
		button_1.setBounds(243, 461, 86, 22);
		frame.getContentPane().add(button_1);
	}

}
