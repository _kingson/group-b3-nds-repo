import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Button;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;

public class ViewDeliveryDetails {

	private JFrame frame;
	private DatabaseConnector dbc;
	private static String dlUsername;
	private DeliveryPerson deliveryPerson;
	private JLabel lblNewLabel;
	private JLabel label;
	private JTextArea txtrSearchForCustomer;
	private JLabel lblSeachForDelivery;
	private JLabel label_1;
	/**
	 * Launch the application.
	 */
	public static void DeliveryDetailsScreen(DatabaseConnector dbc, String nm) {
		dlUsername = nm;
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewDeliveryDetails window = new ViewDeliveryDetails(dbc);
					window.frame.setVisible(true);
					window.loadDetails(dlUsername);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @param dbc 
	 * @throws Exception 
	 */
	public ViewDeliveryDetails(DatabaseConnector dbc) throws Exception {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws Exception 
	 */
	private void initialize() throws Exception {
		dbc = new DatabaseConnector();
		deliveryPerson = new DeliveryPerson(dbc);
		frame = new JFrame("View Delivery Details");
		frame.setBounds(100, 100, 508, 478);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblViewCustomers = new JLabel("View Delivery Person");
		lblViewCustomers.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblViewCustomers.setBounds(140, 11, 224, 31);
		frame.getContentPane().add(lblViewCustomers);
		
		JLabel lblName_1 = new JLabel("Name:");
		lblName_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblName_1.setBounds(10, 102, 78, 17);
		frame.getContentPane().add(lblName_1);
		
		JLabel lblDetails = new JLabel("Details");
		lblDetails.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblDetails.setBounds(201, 43, 78, 31);
		frame.getContentPane().add(lblDetails);
		
		lblNewLabel = new JLabel("Seach for delivery person...");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel.setBounds(140, 104, 224, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblAddress = new JLabel("Address:");
		lblAddress.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblAddress.setBounds(10, 199, 78, 17);
		frame.getContentPane().add(lblAddress);
		
		txtrSearchForCustomer = new JTextArea();
		txtrSearchForCustomer.setEnabled(false);
		txtrSearchForCustomer.setLineWrap(true);
		txtrSearchForCustomer.setText("Search for delivery person...");
		txtrSearchForCustomer.setBounds(140, 197, 189, 60);
		frame.getContentPane().add(txtrSearchForCustomer);
		
		JLabel lblPhone = new JLabel("Phone:");
		lblPhone.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPhone.setBounds(10, 293, 78, 17);
		frame.getContentPane().add(lblPhone);
		
		lblSeachForDelivery = new JLabel("Seach for delivery person...");
		lblSeachForDelivery.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblSeachForDelivery.setBounds(140, 295, 224, 14);
		frame.getContentPane().add(lblSeachForDelivery);
		
		JLabel lblPublications = new JLabel("Username:");
		lblPublications.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPublications.setBounds(10, 146, 96, 17);
		frame.getContentPane().add(lblPublications);
		
		Button button_1 = new Button("<< Return");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					DeliveryPersonMenu nw=new DeliveryPersonMenu(dbc);
					nw.DeliveryPersonMenuScreen(dbc, dlUsername);
				} catch(Exception ex) {
					ex.printStackTrace();
				}
				try {
					frame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		button_1.setBounds(59, 396, 86, 22);
		frame.getContentPane().add(button_1);
		
		label = new JLabel("Seach for delivery person...");
		label.setFont(new Font("Tahoma", Font.PLAIN, 13));
		label.setBounds(140, 148, 224, 14);
		frame.getContentPane().add(label);
		
		JLabel lblAssignedRegion = new JLabel("Assigned Region:");
		lblAssignedRegion.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblAssignedRegion.setBounds(10, 347, 119, 17);
		frame.getContentPane().add(lblAssignedRegion);
		
		label_1 = new JLabel("Seach for delivery person...");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		label_1.setBounds(140, 350, 224, 14);
		frame.getContentPane().add(label_1);
		
	}

	public void loadDetails(String s) throws Exception {
		ResultSet rs = deliveryPerson.viewDeliveryPersonDetails(dlUsername);
		rs.next();
		lblNewLabel.setText(rs.getString("name"));
		label.setText(rs.getString("username"));
		txtrSearchForCustomer.setText(rs.getString("address"));
		lblSeachForDelivery.setText(rs.getString("phone_number"));
		label_1.setText(rs.getString("assigned_regions"));
		
	}
}
