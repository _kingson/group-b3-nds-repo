import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PublicationChoose extends JFrame {

	private JPanel contentPane;
	private JFrame myFrame;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DeliveryChoose frame = new DeliveryChoose();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/
	public static void PublicationchooseMenu(DatabaseConnector dbobj) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PublicationChoose frame = new PublicationChoose(dbobj);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PublicationChoose(DatabaseConnector dbobj) {
		super("Publication Choose");
		myFrame = this;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 460, 422);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnAddNewDeliery = new JButton("Add New Publication");
		btnAddNewDeliery.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Pubform nw=new Pubform(dbobj);
					nw.NewScreen11(dbobj);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
				try {
					myFrame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		btnAddNewDeliery.setFont(new Font("����", Font.PLAIN, 12));
		btnAddNewDeliery.setBounds(86, 72, 228, 67);
		contentPane.add(btnAddNewDeliery);
		
		JButton btnSearchDeliveryPerson = new JButton("Search Publications");
		btnSearchDeliveryPerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					ViewPublication nw=new ViewPublication(dbobj);
				nw.ViewPublicationScreen(dbobj);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
				try {
					myFrame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		
		
		
		btnSearchDeliveryPerson.setFont(new Font("����", Font.PLAIN, 14));
		btnSearchDeliveryPerson.setBounds(86, 150, 228, 67);
		contentPane.add(btnSearchDeliveryPerson);
		
		
		JButton btnEditDeliveryPerson = new JButton("Edit/Remove Publications");
		btnEditDeliveryPerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					EditPublication nw=new EditPublication(dbobj);
					nw.EditPublicationScreen(dbobj);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
				try {
					myFrame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		
		
		
		btnEditDeliveryPerson.setFont(new Font("����", Font.PLAIN, 14));
		btnEditDeliveryPerson.setBounds(86, 224, 228, 67);
		contentPane.add(btnEditDeliveryPerson);
		
		
		JButton button_2 = new JButton("BACK");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				choosepage nw=new choosepage(dbobj);
				nw.NewScreen3(dbobj);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
				try {
					myFrame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		button_2.setFont(new Font("����", Font.PLAIN, 16));
		button_2.setBounds(147, 302, 106, 36);
		contentPane.add(button_2);
	}

}
